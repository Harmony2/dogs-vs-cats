import matplotlib.pyplot as plt
from keras import models
from keras import layers
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing import image
from IPython.display import display, Image
from keras.applications import VGG16
import os
import time

conv = VGG16(weights='imagenet', include_top=False, input_shape=(150, 150, 3))

train_datagen = ImageDataGenerator(rescale=1./255, rotation_range=0.2, width_shift_range=0.2, height_shift_range=0.2, shear_range=0.2, horizontal_flip=True, zoom_range=0.2, fill_mode="nearest")
test_datagen = ImageDataGenerator(rescale=1./255)

train_generator = train_datagen.flow_from_directory("D:\\scripts\\dogs-vs-cats\\dataset\\train\\", target_size=(150,150), batch_size=20, class_mode="binary")
validation_generator = test_datagen.flow_from_directory("D:\\scripts\\dogs-vs-cats\\dataset\\validate\\", target_size=(150,150), batch_size=20, class_mode="binary")

model = models.Sequential()
model.add(conv)
model.add(layers.Flatten())
model.add(layers.Dense(256, activation="relu"))
model.add(layers.Dense(1, activation="sigmoid"))

print("\n\n\n\n")
print("Influentiable weights: ", len(model.trainable_weights))
print("[...] Freezing convolutional base")

conv.trainable = False

print("[+] freezed convolutional base\nInfluentiable weights:", len(model.trainable_weights))

time.sleep(5)

model.compile(loss="binary_crossentropy", optimizer=optimizers.RMSprop(lr=2e-5), metrics=['acc'])

history = model.fit_generator(train_generator, steps_per_epoch=100, epochs=30, validation_data=validation_generator, validation_steps=50)

model.save("cats_vs_dogs_VGG16_1.h5")

acc = history.history["acc"]
val_acc = history.history["val_acc"]

epochs = range(1,len(acc)+1)

plt.plot(epochs, acc, "blue", label="Training")
plt.plot(epochs, val_acc, "green", label="Validation")
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
plt.show()